<!DOCTYPE html>
<html class="no-js" <?php language_attributes(); ?>>
	<head>
		<meta charset="UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="profile" href="http://gmpg.org/xfn/11">
		<meta name="flattr:id" content="49zjz5">
		<meta name="monetization" content="$coil.xrptipbot.com/9G2gnk_1QVmUgKRtSu1BKg">
		<link rel="alternate" type="application/atom+xml" href="https://shkspr.mobi/blog/feed/atom/" />
		<?php wp_head(); ?>
	</head>

	<body <?php body_class(); ?>>

		<header id="masthead" class="site-header">
			<?php
				if ( is_single() ): ?>
				<nav class="site-title"><a href="<?php echo esc_url( home_url() ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></nav>
			<?php
				else: ?>
				<h1  class="site-title"><a href="<?php echo esc_url( home_url() ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
			<?php
				endif;
			?>
		</header>

		<main class="wrapper" id="primary">
			<?php
			if ( have_posts() )  :
				while ( have_posts() ) : the_post(); ?>
					<article <?php post_class( 'post' ); ?>>
						<header class="entry-header">
						<?php
							if ( is_single() ): ?>
							<h1 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
						<?php
							else: ?>
							<h2 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
						<?php
							endif;
							edent_byline(); ?>
							<hr class="hr-top"/>
						</header>
						<div class="entry-content">
							<?php
							if ( is_single() || is_page()) :
								the_content();
							else :
								if ( has_post_thumbnail() ) :
									echo '<a href="' . esc_url( get_permalink() ) .'">'.get_the_post_thumbnail(get_the_ID(),'full').'</a>';
								endif;
								the_excerpt();
								echo  '<a class="moretag" href="'. get_permalink() . '">Continue reading →</a>';
							endif;?>
						</div>
					</article>

					<?php
				endwhile;
			else : ?>
				<div class="hentry">
					<h2><?php _e( 'Sorry, the page you requested cannot be found.', 'edent' ); ?></h2>
					<h2><?php _e( 'These are not the blogs you\'re looking for…', 'edent' ); ?></h2>
					<video height="268" width="480" autoplay loop muted="">
						<source src="/blog/wp-content/themes/edent-wordpress-theme/assets/videos/404.mp4">
					</video>
				</div>
			<?php
			endif;

			if ( is_singular() ):
				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;
			endif;

			if ( ( ! is_singular() ) && ( $wp_query->post_count >= get_option( 'posts_per_page' ) ) ) : ?>
			<nav class="navigation posts-navigation">
				<h2 class="screen-reader-text">Posts navigation</h2>
				<div class="nav-next" rel="next"><?php previous_posts_link( __( 'Newer posts ▶', 'edent' ) ); ?></div>
				<div class="nav-previous" rel="prev"><?php next_posts_link( __( '◀ Older posts', 'edent' ) ); ?></div>
			</nav>
			<?php
			endif; ?>

		</main>

		<aside id="secondary" class="widget-area">
			<?php dynamic_sidebar( 'sidebar-1' ); ?>
		</aside>

		<footer id="colophon" class="site-footer">
			<nav>
				<?php wp_nav_menu(); ?>
			</nav>
		</footer>
		<?php
			wp_footer();
		?>

	</body>
</html>
