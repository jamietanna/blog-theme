<?php


/* THEME SETUP
------------------------------------------------ */

	function edent_setup() {

		// Automatic feed
		add_theme_support( 'automatic-feed-links' );

		// Set content-width
		global $content_width;
		if ( ! isset( $content_width ) ) $content_width = 620;

		// Post thumbnails
		add_theme_support( 'post-thumbnails' );
		add_image_size( 'post-image', 620, 9999 );

		// Title tag
		add_theme_support( 'title-tag' );

		// Post formats
		add_theme_support( 'post-formats', array( 'aside' ) );

		// Add nav menu
		register_nav_menu( 'primary-menu', __( 'Primary Menu', 'edent' ) );

		// Make the theme translation ready
		load_theme_textdomain( 'edent', get_template_directory() . '/languages' );

		$locale_file = get_template_directory() . "/languages/" . get_locale();

		if ( is_readable( $locale_file ) ) {
			require_once( $locale_file );
		}

	}
	add_action( 'after_setup_theme', 'edent_setup' );



/* ENQUEUE STYLES
------------------------------------------------ */

if ( ! function_exists( 'edent_load_style' ) ) {

	function edent_load_style() {
		wp_enqueue_style( 'edent_style', get_stylesheet_uri(), null, '1.0.57' );
	}
	add_action( 'wp_enqueue_scripts', 'edent_load_style' );

	// REMOVE WP EMOJI
	//	http://www.denisbouquet.com/remove-wordpress-emoji-code/
	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
	remove_action( 'wp_print_styles', 'print_emoji_styles' );
	remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
	remove_action( 'admin_print_styles', 'print_emoji_styles' );

	// https://wordpress.org/support/topic/remove-the-new-dns-prefetch-code/
	add_filter( 'emoji_svg_url', '__return_false' );

}


/* ENQUEUE COMMENT-REPLY.JS
------------------------------------------------ */

if ( ! function_exists( 'edent_load_scripts' ) ) {

	function edent_load_scripts() {
		if ( ( ! is_admin() ) && is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
			wp_enqueue_script( 'comment-reply' );
		}
	}
	add_action( 'wp_enqueue_scripts', 'edent_load_scripts' );

}


/* BODY CLASSES
------------------------------------------------ */

if ( ! function_exists( 'edent_body_classes' ) ) {

	function edent_body_classes( $classes ) {
		return $classes;
	}
	add_action( 'body_class', 'edent_body_classes' );

}


/* CUSTOMIZER SETTINGS
------------------------------------------------ */

class edent_customize {

	public static function edent_register ( $wp_customize ) {

		// Make built-in controls use live JS preview
		$wp_customize->get_setting( 'blogname' )->transport = 'postMessage';
		$wp_customize->get_setting( 'background_color' )->transport = 'postMessage';


		// SANITATION

		// Sanitize boolean for checkbox
		function edent_sanitize_checkbox( $checked ) {
			return ( ( isset( $checked ) && true == $checked ) ? true : false );
		}

	}

	// Initiate the live preview JS
	public static function edent_live_preview() {
		wp_enqueue_script( 'edent-themecustomizer', get_template_directory_uri() . '/assets/js/theme-customizer.js', array(  'jquery', 'customize-preview' ), '', true );
	}

}

// Setup the Theme Customizer settings and controls
add_action( 'customize_register', array( 'edent_customize', 'edent_register' ) );

// Enqueue live preview javascript in Theme Customizer admin screen
add_action( 'customize_preview_init', array( 'edent_customize' , 'edent_live_preview' ) );

// Replaces the excerpt "Read More" text by a link
function new_excerpt_more($more) {
	// global $post;
	// return '… <a class="moretag" href="'. get_permalink($post->ID) . '">Continue reading →</a>';
}
add_filter('excerpt_more', 'new_excerpt_more');
/* ---------------------------------------------------------------------------------------------
   SPECIFY GUTENBERG SUPPORT
------------------------------------------------------------------------------------------------ */


if ( ! function_exists( 'edent_add_gutenberg_features' ) ) :

	function edent_add_gutenberg_features() {

		/* Gutenberg Palette --------------------------------------- */
		//	https://wordpress.org/gutenberg/handbook/designers-developers/developers/themes/theme-support/#enqueuing-the-editor-style
		add_theme_support( 'editor-styles' );
		add_editor_style( 'style-editor.css' );

		add_theme_support( 'editor-color-palette', array(
			array(
				'name' 	=> _x( 'Black', 'Name of the black color in the Gutenberg palette', 'edent' ),
				'slug' 	=> 'black',
				'color' => '#000',
			),
			array(
				'name' 	=> _x( 'White', 'Name of the white color in the Gutenberg palette', 'edent' ),
				'slug' 	=> 'white',
				'color' => '#fff',
			),
		) );

	}
	add_action( 'after_setup_theme', 'edent_add_gutenberg_features' );

endif;

//	My Stuff
function edent_byline() {
	$author = get_the_author();
	$byline =    '<span class="posted-on">'.
                    '<time
                        class="entry-date published updated"
                        datetime="' . get_the_time('Y-m-d') . '" >' .
                        get_the_time(get_option('date_format')) .
                    '</time>'.
                  '</span> by ' .
                  '<span class="vcard author">
							<span class="fn">'. $author .'</span>
						</span>';

	$tags_list = get_the_tag_list( '#', esc_html__( ' #', 'edent_simple' ) );

	if ( $tags_list ) {
		$byline .= sprintf( '<span class="sep"> | </span>
		                     <span class="tags-links">' .
		                        esc_html__( '%1$s', 'edent_simple' ) .
		                     '</span>', $tags_list ); // WPCS: XSS OK.
	}

	$num_comments = get_comments_number(); // get_comments_number returns only a numeric value

	if ( comments_open() ) {
		if ( $num_comments == 0 ) {
			// $comments = 'No Comments';
		} elseif ( $num_comments > 1 ) {
			$comments = $num_comments . ' comments';
		} else {
			$comments = '1 comment';
		}

		if ($num_comments >= 1) {
			$byline .= '<span class="sep"> | </span><a href="' . get_comments_link() .'">'. $comments.'</a>';
		}
	}

	if( function_exists( 'stats_get_csv' ) ) {
		$views = get_post_meta( get_the_ID(), 'jetpack-post-views', true );
		$stats_query = "edent_stats_for_" . get_the_ID();
		if ( false === ( $special_query_results = get_transient( $stats_query ) ) ) {
			$stat_options = "post_id=".get_the_ID()."&days=-1";
			$post_stats = stats_get_csv( 'postviews', $stat_options );
			// It wasn't there, so regenerate the data and save the transient
			$special_query_results = $post_stats[0]["views"];
			set_transient( $stats_query, $special_query_results, 6 * HOUR_IN_SECONDS );
		}
		if (($special_query_results != null) && ($special_query_results > 100))
		{
			$byline .= '<span class="sep"> | </span>Read ~' . number_format($special_query_results) . " times.";
		}
	}

	echo $byline;
}

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function edent_simple_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'edent_simple' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'edent_simple' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'edent_simple_widgets_init' );
?>
